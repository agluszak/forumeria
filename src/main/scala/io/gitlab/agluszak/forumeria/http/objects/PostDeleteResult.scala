package io.gitlab.agluszak.forumeria.http.objects

sealed trait PostDeleteResult
object PostDeleteResult {

  case object PostNotFound extends PostDeleteResult

  case object CannotDeleteFirstPost extends PostDeleteResult

  case object Success extends PostDeleteResult

  case object WrongSecret extends PostDeleteResult

}