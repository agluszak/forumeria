package io.gitlab.agluszak.forumeria.http.directives

import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.Directives._
import io.gitlab.agluszak.forumeria.http.JsonSupport
import io.gitlab.agluszak.forumeria.http.rejections.ModelValidationRejection

final case class FieldErrorInfo(field: String, error: String)

trait Validator[T] extends (T => Seq[FieldErrorInfo])

object ValidationDirective extends JsonSupport {

  def validateModel[T](model: T)(implicit validator: Validator[T]): Directive0 = {
    validator(model) match {
      case Nil => pass
      case errors: Seq[FieldErrorInfo] => reject(ModelValidationRejection(errors))
    }
  }

}
