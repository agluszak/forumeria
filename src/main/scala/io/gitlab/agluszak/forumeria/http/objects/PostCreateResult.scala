package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.Post

sealed trait PostCreateResult
object PostCreateResult {
  case class Success(post: Post) extends PostCreateResult
  case object TopicNotFound extends PostCreateResult
}
