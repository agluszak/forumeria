package io.gitlab.agluszak.forumeria.http

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, _}
import akka.http.scaladsl.server.{ExceptionHandler, MalformedRequestContentRejection, RejectionHandler}
import io.gitlab.agluszak.forumeria.http.rejections._
import spray.json._

case class ErrorResponse(errorCode: Int, error: String, details: Option[JsValue] = None)

object ErrorCodes {
  val TOPIC_NOT_FOUND = 1
  val POST_NOT_FOUND = 2
  val CANNOT_DELETE_FIRST_POST = 3
  val WRONG_SECRET = 4
  val MODEL_VALIDATION = 5
  val MALFORMED_REQUEST = 6
  val OTHER = 314
  val WRONG_URL = 315
  val BUSINESS_LOGIC_ERROR = 666
}
object ErrorHandlers extends JsonSupport {
  val rejectionHandler: RejectionHandler = RejectionHandler.newBuilder().handle {
    case ModelValidationRejection(fieldErrors) =>
      complete(StatusCodes.BadRequest,
        ErrorResponse(ErrorCodes.MODEL_VALIDATION, "Model validation error", Some(fieldErrors.toJson)))
    case TopicNotFoundRejection =>
      complete(StatusCodes.NotFound,
        ErrorResponse(ErrorCodes.TOPIC_NOT_FOUND, "No topic with such ID"))
    case PostNotFoundRejection =>
      complete(StatusCodes.NotFound,
        ErrorResponse(ErrorCodes.POST_NOT_FOUND, "No post with such ID"))
    case CannotDeleteFirstPostRejection =>
      complete(StatusCodes.Conflict,
        ErrorResponse(ErrorCodes.CANNOT_DELETE_FIRST_POST,
          "Cannot delete delete first post in topic. Delete topic instead"))
    case WrongSecretRejection =>
      complete(StatusCodes.Forbidden,
        ErrorResponse(ErrorCodes.WRONG_SECRET, "Provided secret is incorrect"))
    case MalformedRequestContentRejection(message, _) =>
      complete(StatusCodes.BadRequest,
        ErrorResponse(ErrorCodes.MALFORMED_REQUEST, "Request is malformed", Some(message.toJson)))
  }.handleNotFound {
    extractUnmatchedPath { p =>
      val detailsJson = JsObject(("requestedPath", JsString(p.toString())))
      complete(StatusCodes.NotFound,
        ErrorResponse(ErrorCodes.WRONG_URL, "Requested path does not exist", Some(detailsJson)))
    }
  }.result().withFallback(RejectionHandler.default.mapRejectionResponse {
    case response@HttpResponse(_, _, ent: HttpEntity.Strict, _) =>
      response.withEntity(
        ErrorResponse(ErrorCodes.OTHER, "Other error", Some(ent.data.utf8String.toJson)).toJson.prettyPrint)
  })

  val exceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: Throwable =>
      val detailsJson = JsObject(("message", JsString(e.getMessage)))
      complete(StatusCodes.InternalServerError,
        ErrorResponse(ErrorCodes.BUSINESS_LOGIC_ERROR, "Internal server error", Some(detailsJson)))
  }
}