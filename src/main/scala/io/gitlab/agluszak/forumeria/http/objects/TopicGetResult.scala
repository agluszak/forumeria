package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.Topic

sealed trait TopicGetResult

object TopicGetResult {

  case class Success(topic: Topic) extends TopicGetResult

  case object TopicNotFound extends TopicGetResult

}
