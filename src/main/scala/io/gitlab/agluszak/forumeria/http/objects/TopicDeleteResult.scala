package io.gitlab.agluszak.forumeria.http.objects

sealed trait TopicDeleteResult
object TopicDeleteResult {

  case object TopicNotFound extends TopicDeleteResult

  case object Success extends TopicDeleteResult

  case object WrongSecret extends TopicDeleteResult

}