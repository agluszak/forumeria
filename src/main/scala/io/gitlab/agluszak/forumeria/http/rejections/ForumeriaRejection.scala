package io.gitlab.agluszak.forumeria.http.rejections

import akka.http.scaladsl.server.Rejection
import io.gitlab.agluszak.forumeria.http.directives.FieldErrorInfo

sealed trait ForumeriaRejection extends Rejection

final case class ModelValidationRejection(invalidFields: Seq[FieldErrorInfo]) extends ForumeriaRejection

case object TopicNotFoundRejection extends ForumeriaRejection

case object PostNotFoundRejection extends ForumeriaRejection

case object CannotDeleteFirstPostRejection extends ForumeriaRejection

case object WrongSecretRejection extends ForumeriaRejection
