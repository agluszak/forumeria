package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.PostWithoutSecret

sealed trait PostGetResult

object PostGetResult {

  case class Success(post: PostWithoutSecret) extends PostGetResult

  case object PostNotFound extends PostGetResult

}
