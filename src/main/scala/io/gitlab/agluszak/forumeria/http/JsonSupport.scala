package io.gitlab.agluszak.forumeria.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import io.gitlab.agluszak.forumeria.http.directives.FieldErrorInfo
import io.gitlab.agluszak.forumeria.http.objects.TopicCreateResponse
import io.gitlab.agluszak.forumeria.models._
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  import io.gitlab.agluszak.forumeria.services.FormatService._
  implicit val postFormat = jsonFormat8(Post.apply)
  implicit val postWithoutSecretFormat = jsonFormat7(PostWithoutSecret.apply)
  implicit val postCreateInputFormat = jsonFormat4(PostCreateInput.apply)
  implicit val postEditInputFormat = jsonFormat1(PostEditInput.apply)
  implicit val topicFormat = jsonFormat4(Topic.apply)
  implicit val topicInputFormat = jsonFormat4(TopicCreateInput.apply)
  implicit val topicPostResponseFormat = jsonFormat2(TopicCreateResponse.apply)
  implicit val validatedFieldFormat = jsonFormat2(FieldErrorInfo)
  implicit val errorResponseFormat = jsonFormat3(ErrorResponse.apply)
}
