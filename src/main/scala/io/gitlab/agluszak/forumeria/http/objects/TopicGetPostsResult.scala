package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.PostWithoutSecret

sealed trait TopicGetPostsResult
object TopicGetPostsResult {
  case object PivotNotFound extends TopicGetPostsResult
  case object TopicNotFound extends TopicGetPostsResult
  case class Success(posts: Seq[PostWithoutSecret]) extends TopicGetPostsResult
}
