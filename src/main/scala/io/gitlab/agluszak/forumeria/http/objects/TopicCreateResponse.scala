package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.{Post, Topic}

case class TopicCreateResponse(topic: Topic, post: Post)
