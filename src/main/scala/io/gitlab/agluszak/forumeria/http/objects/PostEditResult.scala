package io.gitlab.agluszak.forumeria.http.objects

import io.gitlab.agluszak.forumeria.models.PostWithoutSecret

sealed trait PostEditResult
object PostEditResult {
  case class Success(post: PostWithoutSecret) extends PostEditResult

  case object PostNotFound extends PostEditResult
  case object WrongSecret extends PostEditResult
}