package io.gitlab.agluszak.forumeria

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import io.gitlab.agluszak.forumeria.controllers.{PostController, TopicController}
import io.gitlab.agluszak.forumeria.http.ErrorHandlers
import io.gitlab.agluszak.forumeria.repositiories.{DAO, PostRepository, TopicRepository}
import io.gitlab.agluszak.forumeria.services._
import org.apache.log4j.Level
import org.apache.log4j.ConsoleAppender
import org.apache.log4j.Logger
import org.apache.log4j.PatternLayout
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.io.StdIn

object WebServer extends App
  with ConfigService
  with WebApi  {
  override implicit val system: ActorSystem = ActorSystem("Forumeria")
  override implicit val executor: ExecutionContext = system.dispatcher
  override implicit val materializer: ActorMaterializer = ActorMaterializer()



  val root = Logger.getRootLogger
  root.setLevel(Level.INFO)
  root.addAppender(new ConsoleAppender(new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN)))

  val databaseService = new PostgresService
  val dao = new DAO(databaseService)
  val postRepository = new PostRepository(dao)
  val topicRepository = new TopicRepository(dao)
  val postService = new PostService(secretLength, databaseService, postRepository, topicRepository)
  val topicService = new TopicService(secretLength, defaultPosts, maxPosts, defaultTopics, maxTopics,
    databaseService, topicRepository, postRepository)
  val validationService = new ValidationService(maxEmailLength, maxMessageLength, maxNicknameLength, maxTopicNameLength)
  val topicController = new TopicController(topicService, validationService)
  val postController = new PostController(postService, validationService)

  implicit val rejectionHandler = ErrorHandlers.rejectionHandler
  implicit val exceptionHandler = ErrorHandlers.exceptionHandler


  Await.result(databaseService.db.run(dao.createTablesIfTheyDoNotExist), 10.seconds)
  val bindingFuture = Http().bindAndHandle(topicController.routes ~ postController.routes, httpHost, httpPort)

  println(s"Server online at $httpHost:$httpPort/\nPress RETURN to stop...")
  StdIn.readLine()
  bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}