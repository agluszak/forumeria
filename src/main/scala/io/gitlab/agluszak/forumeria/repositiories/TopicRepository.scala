package io.gitlab.agluszak.forumeria.repositiories

import io.gitlab.agluszak.forumeria.models.{Post, PostWithoutSecret, Topic}

class TopicRepository(val dao: DAO) {

  import dao._
  import dao.databaseService.driver.api._

  def search(limit: Int, offset: Int): DBIO[Seq[Topic]] = {
    topics.sortBy(_.lastActiveDate.reverse).drop(offset).take(limit).result
  }

  def size: DBIO[Int] = topics.size.result

  def save(topic: Topic): DBIO[Topic] = {
    (topics returning topics.map(_.id) into ((topic, id) => topic.copy(id = Some(id)))) += topic
  }

  def saveFirstPost(topicId: Long, postId: Long): DBIO[Int] = {
    firstPosts += (topicId, postId)
  }

  def getFirstPost(topicId: Long): DBIO[Option[Post]] = {
    (for {
      (_, post) <- firstPosts.filter(_.topicId === topicId) join posts on (_.postId === _.id)
    } yield post).result.headOption
  }


  def get(id: Long): DBIO[Option[Topic]] = {
    topics.filter(_.id === id).result.headOption
  }

  def getPostsBefore(post: PostWithoutSecret, topic: Topic, count: Int): DBIO[Seq[PostWithoutSecret]] = {
    posts.filter(_.topicId === topic.id.get).sortBy(_.createdDate.reverse)
      .filter(_.createdDate < post.createdDate).take(count).sortBy(_.createdDate)
      .map(_.withoutSecret).result
  }

  def getPostsAfter(post: PostWithoutSecret, topic: Topic, count: Int) : DBIO[Seq[PostWithoutSecret]] = {
    posts.filter(_.topicId === topic.id.get).sortBy(_.createdDate)
      .filter(_.createdDate > post.createdDate).take(count).map(_.withoutSecret).result
  }

  def delete(id: Long): DBIO[Int] = {
    topics.filter(_.id === id).delete
  }

}