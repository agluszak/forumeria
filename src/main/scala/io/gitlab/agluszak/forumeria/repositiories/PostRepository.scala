package io.gitlab.agluszak.forumeria.repositiories

import java.sql.Timestamp

import io.gitlab.agluszak.forumeria.models.{Post, PostWithoutSecret, Topic}

import scala.concurrent.ExecutionContext

class PostRepository(val dao: DAO)(implicit executor: ExecutionContext) {

  import dao._
  import dao.databaseService.driver.api._

  def size: DBIO[Int] = posts.size.result

  def search(id: Long, offset: Int, limit: Int): DBIO[Seq[PostWithoutSecret]] = {
    posts.filter(_.topicId === id).sortBy(_.createdDate).drop(offset).take(limit).map(_.withoutSecret).result
  }

  def edit(post: Post, newEditedDate: Timestamp, newMessage: String): DBIO[PostWithoutSecret] = {
    val query = for {p <- posts if p.id === post.id.get} yield (p.message, p.editedDate)
    query
      .update(newMessage, newEditedDate)
      .map(_ =>
        PostWithoutSecret(
          post.id,
          post.topicId,
          post.nickname,
          post.email,
          post.createdDate,
          newEditedDate,
          newMessage
        ))
    }

  def get(id: Long): DBIO[Option[Post]] = posts.filter(_.id === id).result.headOption

  def getWithoutSecret(id: Long): DBIO[Option[PostWithoutSecret]] =
    posts.filter(_.id === id).map(_.withoutSecret).result.headOption

  def delete(id: Long): DBIO[Int] = posts.filter(_.id === id).delete

  def save(post: Post): DBIO[Post] =
    (posts returning posts.map(_.id) into ((post, id) => post.copy(id = Some(id)))) += post

  def isFirstPost(post: Post): DBIO[Boolean] = {
    firstPosts
        .filter(firstPost => firstPost.topicId === post.topicId && firstPost.postId === post.id)
        .result
        .headOption
        .map(_.isDefined)
  }

  def updateTopic(topic: Topic, timestamp: Timestamp): DBIO[Int] = {
    val query = for {t <- topics if t.id === topic.id.get} yield t.lastActiveDate
    query.update(timestamp)
  }
}