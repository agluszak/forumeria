package io.gitlab.agluszak.forumeria.repositiories

import java.sql.Timestamp

import io.gitlab.agluszak.forumeria.models.{Post, PostWithoutSecret, Topic}
import io.gitlab.agluszak.forumeria.services.DatabaseService
import slick.jdbc.meta.MTable

import scala.concurrent.ExecutionContext

class DAO(val databaseService: DatabaseService)(implicit executor: ExecutionContext) {

  import databaseService.driver.api._

  lazy val topics = TableQuery[Topics]
  lazy val posts = TableQuery[Posts]
  lazy val firstPosts = TableQuery[FirstPosts]
  val schema = topics.schema ++ posts.schema ++ firstPosts.schema

  def createTables: DBIO[Unit] = schema.create

  def createTablesIfTheyDoNotExist: DBIO[Unit] = {
    val tables = List(topics, posts, firstPosts)
    MTable.getTables.flatMap(existingTables => {
      val names = existingTables.map(table => table.name.name)
      val createIfNotExist = tables.filter(table =>
        !names.contains(table.baseTableRow.tableName)).map(_.schema.create)

      DBIO.sequence(createIfNotExist).map(_ => ())
    })
  }

  def dropTables: DBIO[Unit] = schema.drop

  class Topics(tag: Tag) extends Table[Topic](tag, "topics") {
    def * = (id.?, name, createdDate, lastActiveDate).mapTo[Topic]

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def createdDate = column[Timestamp]("createdDate")

    def lastActiveDate = column[Timestamp]("lastActiveDate")
  }

  class Posts(tag: Tag) extends Table[Post](tag, "posts") {
    def * = (id.?, topicId, nickname, email, createdDate, editedDate, message, secret).mapTo[Post]

    def withoutSecret =
      (id.?, topicId, nickname, email, createdDate, editedDate, message).mapTo[PostWithoutSecret]

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def topicId = column[Long]("topicId")

    def nickname = column[String]("nickname")

    def email = column[String]("email")

    def createdDate = column[Timestamp]("createdDate")

    def editedDate = column[Timestamp]("editedDate")

    def message = column[String]("message")

    def secret = column[String]("secret")

    def topic =
      foreignKey("topic_fk", topicId, topics)(_.id, onDelete = ForeignKeyAction.Cascade)

  }

  class FirstPosts(tag: Tag) extends Table[(Long, Long)](tag, "firstPost") {
    def * = (topicId, postId)

    def postId = column[Long]("postId", O.Unique)

    def topicId = column[Long]("topicId", O.Unique)

    def pk = primaryKey("topic_post_pk", (topicId, postId))

    def topic =
      foreignKey("firstPost_topic_fk", topicId, topics)(_.id, onDelete = ForeignKeyAction.Cascade)

    def post =
      foreignKey("firstPost_post_fk", postId, posts)(_.id, onDelete = ForeignKeyAction.Cascade)
  }

}
