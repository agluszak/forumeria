package io.gitlab.agluszak.forumeria.utils

import java.security.SecureRandom
import java.sql.Timestamp
import java.time.Instant

import io.gitlab.agluszak.forumeria.models.{Post, Topic}

object Utils {
  private val random: SecureRandom = new SecureRandom()

  def randomSecret(length: Int): String = {
    val chars: Array[Char] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray
    (0 to length).map(_ => chars(random.nextInt(chars.length))).mkString
  }

  def buildPost(topicId: Long, message: String, nickname: String, email: String, secret: String): Post = {
    val timestamp = Timestamp.from(Instant.now())
    Post(None, topicId, nickname, email, timestamp, timestamp, message, secret)
  }

  def buildTopic(name: String): Topic = {
    val timestamp = Timestamp.from(Instant.now())
    Topic(None, name, timestamp, timestamp)
  }

  //  constant time string compare
  def safeStringCompare(a: String, b: String): Boolean = {
    if (a.length != b.length) false
    else a.zip(b).foldLeft(true) { case (acc, (c1, c2)) => acc && c1 == c2 }
  }

  def validateEmail(email: String): Boolean = {
    val emailRegex = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r
    email match {
      case null => false
      case e if e.trim.isEmpty => false
      case e if emailRegex.findFirstMatchIn(e).isDefined => true
      case _ => false
    }
  }
}
