package io.gitlab.agluszak.forumeria.services

import java.sql.Timestamp

import spray.json.{DeserializationException, JsString, JsValue, RootJsonFormat}

object FormatService {

  implicit object DateJsonFormat extends RootJsonFormat[Timestamp] {
    def write(timestamp: Timestamp) = JsString(timestamp.toString)

    def read(value: JsValue): Timestamp = value match {
      case JsString(timestampStr) => Timestamp.valueOf(timestampStr)
      case _ => throw DeserializationException("Date expected")
    }
  }

}
