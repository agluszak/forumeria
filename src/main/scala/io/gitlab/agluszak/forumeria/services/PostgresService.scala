package io.gitlab.agluszak.forumeria.services

import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcProfile, PostgresProfile}

class PostgresService extends DatabaseService with ConfigService {
  val driver: JdbcProfile = PostgresProfile
  val db: Database = Database.forURL(jdbcUrl, dbUser, dbPassword)
  db.createSession()
}
