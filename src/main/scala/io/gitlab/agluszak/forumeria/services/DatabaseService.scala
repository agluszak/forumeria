package io.gitlab.agluszak.forumeria.services

import slick.jdbc.JdbcProfile

trait DatabaseService {
  val driver: JdbcProfile
  import driver.api._
  val db: Database
}
