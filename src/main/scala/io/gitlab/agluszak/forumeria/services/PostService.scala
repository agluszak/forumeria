package io.gitlab.agluszak.forumeria.services

import java.sql.Timestamp
import java.time.Instant

import io.gitlab.agluszak.forumeria.http.objects.{PostCreateResult, PostDeleteResult, PostEditResult, PostGetResult}
import io.gitlab.agluszak.forumeria.repositiories.{PostRepository, TopicRepository}
import io.gitlab.agluszak.forumeria.utils.Utils

import scala.concurrent.{ExecutionContext, Future}

class PostService(secretLength: Int,
                  databaseService: DatabaseService,
                  postRepository: PostRepository,
                  topicRepository: TopicRepository)
                 (implicit executor: ExecutionContext) {

  import databaseService.db

  def create(topicId: Long, nickname: String, email: String, message: String): Future[PostCreateResult] = {
    import databaseService.driver.api._
    val secretForPost = Utils.randomSecret(secretLength)
    val postToBeCreated = Utils.buildPost(topicId, message, nickname, email, secretForPost)
    val timestamp = Timestamp.from(Instant.now())
    val dbAction = topicRepository.get(topicId).flatMap {
      case None => DBIO.successful(PostCreateResult.TopicNotFound)
      case Some(topic) => for {
        _ <- postRepository.updateTopic(topic, timestamp)
        post <- postRepository.save(postToBeCreated)
      } yield PostCreateResult.Success(post)
    }
    db.run(dbAction.transactionally)
  }


  def edit(postId: Long, newMessage: String, secret: String): Future[PostEditResult] = {
    import databaseService.driver.api._
    val timestamp = Timestamp.from(Instant.now())
    val dbAction = postRepository.get(postId).flatMap {
      case None => DBIO.successful(PostEditResult.PostNotFound)
      case Some(post) =>
        if (Utils.safeStringCompare(post.secret, secret))
          postRepository.edit(post, timestamp, newMessage).map(PostEditResult.Success)
        else
          DBIO.successful(PostEditResult.WrongSecret)
    }
    db.run(dbAction.transactionally)
  }



  def delete(postId: Long, secret: String): Future[PostDeleteResult] = {
    import databaseService.driver.api._
    val dbAction = postRepository.get(postId).flatMap {
      case None => DBIO.successful(PostDeleteResult.PostNotFound)
      case Some(post) =>
        postRepository.isFirstPost(post).flatMap {
          case true => DBIO.successful(PostDeleteResult.CannotDeleteFirstPost)
          case false =>
            if (Utils.safeStringCompare(post.secret, secret))
              postRepository.delete(post.id.get).map(_ => PostDeleteResult.Success)
            else
              DBIO.successful(PostDeleteResult.WrongSecret)
        }
    }
    db.run(dbAction.transactionally)
  }

  def size(): Future[Int] = {
    db.run(postRepository.size)
  }

  def get(postId: Long): Future[PostGetResult] = {
    db.run(postRepository.getWithoutSecret(postId)).map {
      case None => PostGetResult.PostNotFound
      case Some(post) => PostGetResult.Success(post)
    }
  }

}

