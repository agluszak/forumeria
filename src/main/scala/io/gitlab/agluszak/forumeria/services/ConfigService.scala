package io.gitlab.agluszak.forumeria.services

import com.typesafe.config.ConfigFactory

trait ConfigService {
  private val config = ConfigFactory.load()
  private val httpConfig = config.getConfig("http")
  private val databaseConfig = config.getConfig("database")
  private val testDatabaseConfig = config.getConfig("testDatabase")
  private val limitsConfig = config.getConfig("limits")


  val httpHost = httpConfig.getString("interface")
  val httpPort = httpConfig.getInt("port")

  val jdbcUrl = databaseConfig.getString("url")
  val dbUser = databaseConfig.getString("user")
  val dbPassword = databaseConfig.getString("password")

  val testDriver = testDatabaseConfig.getString("driver")
  val testJdbcUrl = testDatabaseConfig.getString("url")

  val secretLength = limitsConfig.getInt("secretLength")
  val maxPosts = limitsConfig.getInt("maxPosts")
  val defaultPosts = limitsConfig.getInt("defaultPosts")
  val maxTopics = limitsConfig.getInt("maxTopics")
  val defaultTopics = limitsConfig.getInt("defaultTopics")
  val maxSecretLength = limitsConfig.getInt("maxSecretLength")
  val maxNicknameLength = limitsConfig.getInt("maxNicknameLength")
  val maxEmailLength = limitsConfig.getInt("maxEmailLength")
  val maxMessageLength = limitsConfig.getInt("maxMessageLength")
  val maxTopicNameLength = limitsConfig.getInt("maxTopicNameLength")

}
