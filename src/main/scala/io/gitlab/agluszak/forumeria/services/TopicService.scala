package io.gitlab.agluszak.forumeria.services

import io.gitlab.agluszak.forumeria.http.objects.{TopicCreateResponse, TopicDeleteResult, TopicGetPostsResult, TopicGetResult}
import io.gitlab.agluszak.forumeria.models.Topic
import io.gitlab.agluszak.forumeria.repositiories.{PostRepository, TopicRepository}
import io.gitlab.agluszak.forumeria.utils.Utils

import scala.concurrent.{ExecutionContext, Future}

class TopicService(secretLength: Int,
                   defaultPosts: Int,
                   maxPosts: Int,
                   defaultTopics: Int,
                   maxTopics: Int,
                   databaseService: DatabaseService,
                   topicRepository: TopicRepository,
                   postRepository: PostRepository)
                  (implicit executor: ExecutionContext) {

  import databaseService.db

  def create(name: String, nickname: String, email: String, message: String): Future[TopicCreateResponse] = {
    import databaseService.driver.api._
    val topicToBeCreated = Utils.buildTopic(name)
    val secretForPost = Utils.randomSecret(secretLength)
    val dbAction = for {
      topic <- topicRepository.save(topicToBeCreated)
      postToBeCreated = Utils.buildPost(topic.id.get, message, nickname, email, secretForPost)
      post <- postRepository.save(postToBeCreated)
      _ <- topicRepository.saveFirstPost(topic.id.get, post.id.get)
    } yield TopicCreateResponse(topic, post)
    db.run(dbAction.transactionally)
  }

  def get(id: Long): Future[TopicGetResult] =
    db.run(topicRepository.get(id)).map {
      case Some(topic) => TopicGetResult.Success(topic)
      case None => TopicGetResult.TopicNotFound
    }

  def getPosts(topicId: Long, pivotId: Long, before: Int, after: Int): Future[TopicGetPostsResult] = {
    import databaseService.driver.api._
    val limitedBefore = before.min(maxPosts - 1)
    val limitedAfter = after.min(maxPosts - 1)
    val dbAction = topicRepository.get(topicId).flatMap {
      case None => DBIO.successful(TopicGetPostsResult.TopicNotFound)
      case Some(topic) => postRepository.getWithoutSecret(pivotId).flatMap {
        case None => DBIO.successful(TopicGetPostsResult.PivotNotFound)
        case Some(pivot) => for {
          postsBefore <- topicRepository.getPostsBefore(pivot, topic, limitedBefore)
          postsAfter <- topicRepository.getPostsAfter(pivot, topic, limitedAfter)
        } yield
          if (postsBefore.size + postsAfter.size + 1 <= maxPosts)
            TopicGetPostsResult.Success(postsBefore ++ Seq(pivot) ++ postsAfter)
          else {
            val takeBefore = (maxPosts - 1) * before / (before + after)
            val takeAfter = (maxPosts - 1) - takeBefore
            val takeBeforePosts = postsBefore.takeRight(takeBefore)
            val takeAfterPosts = postsAfter.take(takeAfter)
            TopicGetPostsResult.Success(takeBeforePosts ++ Seq(pivot) ++ takeAfterPosts)
          }
        }
      }
    db.run(dbAction)
  }

  def search(limitOption: Option[Int], offsetOption: Option[Int]): Future[Seq[Topic]] = {
    val limit = limitOption.getOrElse(defaultTopics).min(maxTopics)
    val offset = offsetOption.getOrElse(0)
    db.run(topicRepository.search(limit, offset))
  }

  def delete(id: Long, secret: String): Future[TopicDeleteResult] = {
    import databaseService.driver.api._
    val dbAction = topicRepository.get(id).flatMap {
      case None => DBIO.successful(TopicDeleteResult.TopicNotFound)
      case Some(topic) =>
        topicRepository.getFirstPost(id).flatMap {
          case None => DBIO.successful(TopicDeleteResult.TopicNotFound)
          case Some(post) =>
            if (Utils.safeStringCompare(post.secret, secret))
              topicRepository.delete(id).map(_ => TopicDeleteResult.Success)
            else
              DBIO.successful(TopicDeleteResult.WrongSecret)
        }
    }
    db.run(dbAction)
  }

  def size(): Future[Int] = {
    db.run(topicRepository.size)
  }
}
