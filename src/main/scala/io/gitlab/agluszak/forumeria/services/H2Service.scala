package io.gitlab.agluszak.forumeria.services

import slick.jdbc.H2Profile.api._
import slick.jdbc.{H2Profile, JdbcProfile}

class H2Service extends DatabaseService with ConfigService {
  val driver: JdbcProfile = H2Profile
  val db: Database = Database.forURL(testJdbcUrl, driver = testDriver)
  db.createSession()
}
