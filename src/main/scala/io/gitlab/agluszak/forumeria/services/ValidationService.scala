package io.gitlab.agluszak.forumeria.services

import io.gitlab.agluszak.forumeria.http.directives.{FieldErrorInfo, Validator}
import io.gitlab.agluszak.forumeria.models._
import io.gitlab.agluszak.forumeria.utils.Utils

class ValidationService(maxEmailLength: Int, maxMessageLength: Int, maxNicknameLength: Int, maxTopicNameLength: Int) {
  private def validate(rule: => Boolean, fieldName: String, errorText: String): Option[FieldErrorInfo] =
    if (!rule) Some(FieldErrorInfo(fieldName, errorText)) else None

  private def validateEmail(email: String): Option[FieldErrorInfo] = {
    validate(email.length <= maxEmailLength && Utils.validateEmail(email),
      "email", "Email must be valid"
    )
  }

  private def validateLength(fieldName: String, contents: String,
                             minLength: Int, maxLength: Int): Option[FieldErrorInfo] = {
    validate(contents.length >= minLength && contents.length <= maxLength, fieldName,
      s"$fieldName length must be between $minLength and $maxLength characters")
  }

  private def validateNumberMin(fieldName: String, contents: Long, min: Long) = {
    validate(contents >= min, fieldName, s"$fieldName must be greater or equal $min")
  }

  implicit object TopicCreateInputValidator extends Validator[TopicCreateInput] {
    override def apply(model: TopicCreateInput): Seq[FieldErrorInfo] = {
      List(validateLength("topicName", model.topicName, 1, maxTopicNameLength),
        validateLength("message", model.message, 1, maxMessageLength),
        validateLength("nickname", model.nickname, 1, maxNicknameLength),
        validateEmail(model.email)).flatten
    }
  }

  implicit object PostCreateInputValidator extends Validator[PostCreateInput] {
    override def apply(model: PostCreateInput): Seq[FieldErrorInfo] = {
      List(validateNumberMin("topicId", model.topicId, 1),
        validateLength("nickname", model.nickname, 1, maxNicknameLength),
        validateLength("message", model.message, 1, maxMessageLength),
        validateEmail(model.email)).flatten
    }
  }

  implicit object PostEditInputValidator extends Validator[PostEditInput] {
    override def apply(model: PostEditInput): Seq[FieldErrorInfo] = {
      List(
        validateLength("message", model.message, 1, maxMessageLength)).flatten
    }
  }

}
