package io.gitlab.agluszak.forumeria.models

import java.sql.Timestamp


case class Topic(id: Option[Long] = None,
                 topicName: String,
                 createdDate: Timestamp,
                 lastActiveDate: Timestamp)

case class TopicCreateInput(topicName: String,
                            nickname: String,
                            email: String,
                            message: String)
