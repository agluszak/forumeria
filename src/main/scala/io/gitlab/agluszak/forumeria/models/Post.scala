package io.gitlab.agluszak.forumeria.models

import java.sql.Timestamp

case class Post(id: Option[Long] = None,
                topicId: Long,
                nickname: String,
                email: String,
                createdDate: Timestamp,
                editedDate: Timestamp,
                message: String,
                secret: String) {
  def withoutSecret: PostWithoutSecret =
    PostWithoutSecret(id, topicId, nickname, email, createdDate, editedDate, message)
}

case class PostWithoutSecret(id: Option[Long] = None,
                topicId: Long,
                nickname: String,
                email: String,
                createdDate: Timestamp,
                editedDate: Timestamp,
                message: String)

case class PostCreateInput(topicId: Long,
                     nickname: String,
                     email: String,
                     message: String)

case class PostEditInput(message: String)
