package io.gitlab.agluszak.forumeria.controllers

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import io.gitlab.agluszak.forumeria.http.JsonSupport
import io.gitlab.agluszak.forumeria.http.directives.ValidationDirective.validateModel
import io.gitlab.agluszak.forumeria.http.objects._
import io.gitlab.agluszak.forumeria.http.rejections.{CannotDeleteFirstPostRejection, PostNotFoundRejection, TopicNotFoundRejection, WrongSecretRejection}
import io.gitlab.agluszak.forumeria.models._
import io.gitlab.agluszak.forumeria.services.{PostService, ValidationService}

class PostController(val postService: PostService, validationService: ValidationService ) extends JsonSupport {

  import validationService._

  val routes = pathPrefix("posts") {
    pathEndOrSingleSlash {
      (post & entity(as[PostCreateInput])) { input =>
        validateModel(input).apply {
          onSuccess(postService.create(input.topicId, input.nickname, input.email, input.message)) {
            case PostCreateResult.TopicNotFound => reject(TopicNotFoundRejection)
            case PostCreateResult.Success(post) => complete(StatusCodes.Created, post)
          }
        }
      }
    } ~
      pathPrefix(LongNumber) { id =>
        pathEndOrSingleSlash {
          get {
            onSuccess(postService.get(id)) {
              case PostGetResult.Success(post) => complete(StatusCodes.OK, post)
              case PostGetResult.PostNotFound => reject(PostNotFoundRejection)
            }
          } ~
          (post & entity(as[PostEditInput])) { input =>
            validateModel(input).apply {
              parameter('secret) { secret =>
                onSuccess(postService.edit(id, input.message, secret)) {
                  case PostEditResult.Success(post) => complete(StatusCodes.OK, post)
                  case PostEditResult.PostNotFound => reject(PostNotFoundRejection)
                  case PostEditResult.WrongSecret => reject(WrongSecretRejection)
                }
              }
            }
          } ~
            (delete & parameter('secret)) { secret =>
              onSuccess(postService.delete(id, secret)) {
                case PostDeleteResult.Success => complete(StatusCodes.NoContent)
                case PostDeleteResult.PostNotFound => complete(StatusCodes.NoContent)
                case PostDeleteResult.CannotDeleteFirstPost => reject(CannotDeleteFirstPostRejection)
                case PostDeleteResult.WrongSecret => reject(WrongSecretRejection)
              }
            }
        }
      }
  }
}
