package io.gitlab.agluszak.forumeria.controllers

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import io.gitlab.agluszak.forumeria.http.JsonSupport
import io.gitlab.agluszak.forumeria.http.directives.ValidationDirective._
import io.gitlab.agluszak.forumeria.http.objects._
import io.gitlab.agluszak.forumeria.http.rejections.{PostNotFoundRejection, TopicNotFoundRejection, WrongSecretRejection}
import io.gitlab.agluszak.forumeria.models._
import io.gitlab.agluszak.forumeria.services.{TopicService, ValidationService}
import spray.json._

class TopicController(topicService: TopicService, validationService: ValidationService) extends JsonSupport {

  import validationService._

  val routes = pathPrefix("topics") {
    pathEndOrSingleSlash {
      (post & entity(as[TopicCreateInput])) { input =>
        validateModel(input).apply {
          complete(StatusCodes.Created, topicService.create(input.topicName,
            input.nickname, input.email, input.message))
        }
      } ~
        (get & parameters(('offset.as[Int].?, 'limit.as[Int].?))) { case (offset, limit) =>
          complete(StatusCodes.OK, topicService.search(limit, offset))
        }
    } ~
      pathPrefix(IntNumber) { topicId =>
        pathEndOrSingleSlash {
          get {
            onSuccess(topicService.get(topicId)) {
              case TopicGetResult.Success(topic) => complete(StatusCodes.OK, topic)
              case TopicGetResult.TopicNotFound => reject(TopicNotFoundRejection)
            }
          } ~
            (delete & parameter('secret)) { secret =>
              onSuccess(topicService.delete(topicId, secret)) {
                  case TopicDeleteResult.Success => complete(StatusCodes.NoContent)
                  case TopicDeleteResult.TopicNotFound => complete(StatusCodes.NoContent)
                  case TopicDeleteResult.WrongSecret => reject(WrongSecretRejection)
                }
            }
        } ~
          pathPrefix("posts") {
            pathEndOrSingleSlash {
              (get & parameters(('pivotId.as[Long], 'before.as[Int], 'after.as[Int]))) {
                case (pivotId, before, after) =>
                onSuccess(topicService.getPosts(topicId, pivotId, before, after)) {
                    case TopicGetPostsResult.Success(posts) => complete(StatusCodes.OK, posts.toJson)
                    case TopicGetPostsResult.TopicNotFound => reject(TopicNotFoundRejection)
                    case TopicGetPostsResult.PivotNotFound => reject(PostNotFoundRejection)
                  }
                }
            }
          }
      }
  }
}
