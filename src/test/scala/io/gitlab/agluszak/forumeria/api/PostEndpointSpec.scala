package io.gitlab.agluszak.forumeria.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import io.gitlab.agluszak.forumeria.controllers.PostController
import io.gitlab.agluszak.forumeria.http.objects.{PostCreateResult, TopicCreateResponse}
import io.gitlab.agluszak.forumeria.http.{ErrorCodes, ErrorResponse}
import io.gitlab.agluszak.forumeria.models.{Post, PostCreateInput, PostEditInput, PostWithoutSecret}

import scala.concurrent.Future

class PostEndpointSpec extends ApiTest {
  val postController = new PostController(postService, validationService)

  "PostController" can {
    "create" which {
      "successfully creates a new post" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, _) =>
          val postCreateInput = PostCreateInput(topic.id.get, "john", "john@doe.com", "hello there")
          Post("/posts/", postCreateInput) ~> postController.routes ~> check {
            status mustBe StatusCodes.Created
            val apiResponse = responseAs[Post]
            apiResponse.id mustBe defined
            apiResponse.message mustBe postCreateInput.message
            apiResponse.nickname mustBe postCreateInput.nickname
            apiResponse.topicId mustBe postCreateInput.topicId
            apiResponse.email mustBe postCreateInput.email
          }
        }
      }

      "fails for wrong topic id" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, _) =>
          val postCreateInput = PostCreateInput(topic.id.get + 1, "john", "john@doe.com", "hello there")
          Post("/posts/", postCreateInput) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.NotFound
            val apiResponse = responseAs[ErrorResponse]
            apiResponse.errorCode mustBe ErrorCodes.TOPIC_NOT_FOUND
          }
        }
      }

      "fails for wrong email" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, _) =>
          val postCreateInput = PostCreateInput(topic.id.get, "john", "doe.com", "hello there")
          Post("/posts/", postCreateInput) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.BadRequest
            val apiResponse = responseAs[ErrorResponse]
            apiResponse.errorCode mustBe ErrorCodes.MODEL_VALIDATION
          }
        }
      }

      "fails for malformed input" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, _) =>
          val postCreateInput = PostEditInput("WRONG INPUT")
          Post("/posts/", postCreateInput) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.BadRequest
            val apiResponse = responseAs[ErrorResponse]
            apiResponse.errorCode mustBe ErrorCodes.MALFORMED_REQUEST
            }
          }
        }
      }

      "fails for non-json input" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, _) =>
          val postCreateInput = "blah blah"
          Post("/posts/", postCreateInput) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.UnsupportedMediaType
          }
        }
      }
    }

    "get" which {
      "returns existing post" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, post) =>
          val id = post.id.get
          Get(s"/posts/$id") ~> postController.routes ~> check {
            status mustBe StatusCodes.OK
            val apiResponse = responseAs[PostWithoutSecret]
            apiResponse.message mustBe post.message
            apiResponse.nickname mustBe post.nickname
            apiResponse.topicId mustBe post.topicId
            apiResponse.email mustBe post.email
          }
        }
      }

      "fails for nonexistent post" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, post) =>
          val id = post.id.get + 1
          Get(s"/posts/$id") ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.NotFound
            val apiResponse = responseAs[ErrorResponse]
            apiResponse.errorCode mustBe ErrorCodes.POST_NOT_FOUND
          }
        }
      }
    }
    
    "edit" which {
      "successfully edits a post" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, post) =>
          val id = post.id.get 
          val secret = post.secret
          val newMessage = "HI"
          Post(s"/posts/$id?secret=$secret", PostEditInput(newMessage)) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.OK
            val apiResponse = responseAs[PostWithoutSecret]
            apiResponse.message mustBe newMessage
          }
        }
      }

      "fails for wrong secret" in {
        topicService.create("hello", "john", "john@doe.com", "hi")
          .flatMap { case TopicCreateResponse(topic, post) =>
          val id = post.id.get
          val secret = post.secret + "x"
          val newMessage = "HI"
          Post(s"/posts/$id?secret=$secret", PostEditInput(newMessage)) ~> Route.seal(postController.routes) ~> check {
            status mustBe StatusCodes.Forbidden
            val apiResponse = responseAs[ErrorResponse]
            apiResponse.errorCode mustBe ErrorCodes.WRONG_SECRET
          }
      }
    }

      "delete" which {
        "successfully deletes a post" in {
          topicService.create("hello", "john", "john@doe.com", "hi")
            .flatMap { case TopicCreateResponse(topic, _) =>
            postService.create(topic.id.get, "john", "john@doe.com", "hi" ).flatMap {
                case PostCreateResult.Success(post) =>
                  val id = post.id.get
                  val secret = post.secret
                  Delete(s"/posts/$id?secret=$secret") ~> Route.seal(postController.routes) ~> check {
                    status mustBe StatusCodes.NoContent
                  }
                case _ => Future.failed(new Error)
              }
          }
        }

        "fails for wrong secret" in {
          topicService.create("hello", "john", "john@doe.com", "hi")
            .flatMap { case TopicCreateResponse(topic, _) =>
            postService.create(topic.id.get, "john", "john@doe.com", "hi" ).flatMap {
              case PostCreateResult.Success(post) =>
                val id = post.id.get
                val secret = post.secret + "x"
                Delete(s"/posts/$id?secret=$secret") ~> Route.seal(postController.routes) ~> check {
                  status mustBe StatusCodes.Forbidden
                  val apiResponse = responseAs[ErrorResponse]
                  apiResponse.errorCode mustBe ErrorCodes.WRONG_SECRET
                }
              case _ => Future.failed(new Error)
            }
          }
        }

        "is idempotent" in {
          topicService.create("hello", "john", "john@doe.com", "hi")
            .flatMap { case TopicCreateResponse(topic, post) =>
            val id = post.id.get
            val secret = post.secret
            topicService.delete(topic.id.get, secret).flatMap { _ =>
              Delete(s"/posts/$id?secret=$secret") ~> Route.seal(postController.routes) ~> check {
                status mustBe StatusCodes.NoContent
              }
            }
          }
        }

        "fails to delete the first post" in {
          topicService.create("hello", "john", "john@doe.com", "hi")
            .flatMap { case TopicCreateResponse(topic, post) =>
            val id = post.id.get
            val secret = post.secret
            Delete(s"/posts/$id?secret=$secret") ~> Route.seal(postController.routes) ~> check {
              status mustBe StatusCodes.Conflict
              val apiResponse = responseAs[ErrorResponse]
              apiResponse.errorCode mustBe ErrorCodes.CANNOT_DELETE_FIRST_POST
            }
          }
        }
      }
  }
}