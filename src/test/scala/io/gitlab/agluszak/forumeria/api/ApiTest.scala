package io.gitlab.agluszak.forumeria.api

import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.gitlab.agluszak.forumeria.http.{ErrorHandlers, JsonSupport}
import io.gitlab.agluszak.forumeria.services.ValidationService
import io.gitlab.agluszak.forumeria.{CommonTest, WebApi}

import scala.concurrent.Await
import scala.concurrent.duration._

class ApiTest extends CommonTest with WebApi with ScalatestRouteTest with JsonSupport {
  override implicit val executor = system.dispatcher
  implicit val rejectionHandler = ErrorHandlers.rejectionHandler
  implicit val exceptionHandler = ErrorHandlers.exceptionHandler
  val validationService = new ValidationService(maxEmailLength,
    maxMessageLength, maxNicknameLength, maxTopicNameLength)

  override def beforeAll: Unit = {
    org.apache.log4j.BasicConfigurator.configure()
    Await.result(databaseService.db.run(dao.createTables), 10.seconds)
  }

  override def afterAll: Unit = {
    Await.result(databaseService.db.run(dao.dropTables), 10.seconds)
  }
}
