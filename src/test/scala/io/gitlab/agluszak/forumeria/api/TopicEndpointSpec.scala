package io.gitlab.agluszak.forumeria.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import io.gitlab.agluszak.forumeria.controllers.{PostController, TopicController}
import io.gitlab.agluszak.forumeria.http.{ErrorCodes, ErrorResponse}
import io.gitlab.agluszak.forumeria.http.objects.{TopicCreateResponse, TopicGetResult}
import io.gitlab.agluszak.forumeria.models._

import scala.concurrent.Future


class TopicEndpointSpec extends ApiTest {
  val topicController = new TopicController(topicService, validationService)

  "PostController" can {
    "create" which {
      "successfully creates a new topic" in {
        val topicCreateInput = TopicCreateInput("hello", "john", "john@doe.com", "hello there")
        Future {
          Post("/topics/", topicCreateInput) ~> topicController.routes ~> check {
            status mustBe StatusCodes.Created
            val apiResponse = responseAs[TopicCreateResponse]
            apiResponse.topic.id mustBe defined
            apiResponse.post.id mustBe defined
            apiResponse.post.message mustBe topicCreateInput.message
            apiResponse.post.nickname mustBe topicCreateInput.nickname
            apiResponse.post.topicId mustBe apiResponse.topic.id.get
            apiResponse.post.email mustBe topicCreateInput.email
          }
        }
      }
    }

    "search" which {
      "returns a list of topics" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { _ =>
          Get("/topics/?offset=0&limit=2") ~> topicController.routes ~> check {
            val _ = responseAs[Seq[Topic]]
            status mustBe StatusCodes.OK
          }
        }
      }
    }

    "get" which {
      "returns a topic" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            val id = topic.id.get
            Get(s"/topics/$id") ~> topicController.routes ~> check {
              status mustBe StatusCodes.OK
              val apiResponse = responseAs[Topic]
              apiResponse mustBe topic
            }
          }
      }

      "fails for nonexistent topic" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            val id = topic.id.get + 1
            Get(s"/topics/$id") ~> Route.seal(topicController.routes) ~> check {
              status mustBe StatusCodes.NotFound
              val apiResponse = responseAs[ErrorResponse]
              apiResponse.errorCode mustBe ErrorCodes.TOPIC_NOT_FOUND
            }
          }
      }
    }

    "delete" which {
      "deletes a topic" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            val id = topic.id.get
            val secret = post.secret
            Delete(s"/topics/$id?secret=$secret") ~> topicController.routes ~> check {
              status mustBe StatusCodes.NoContent
            }
          }
      }

      "fails for wrong secret" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            val id = topic.id.get
            val secret = post.secret + "x"
            Delete(s"/topics/$id?secret=$secret") ~> Route.seal(topicController.routes) ~> check {
              status mustBe StatusCodes.Forbidden
              val apiResponse = responseAs[ErrorResponse]
              apiResponse.errorCode mustBe ErrorCodes.WRONG_SECRET
            }
          }
      }
    }

    "getPosts" which {
      "returns a list of posts" in {
        topicService.create("hello", "john", "john@doe.com", "hello there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            val id = topic.id.get
            val pivotId = post.id.get
            Get(s"/topics/$id/posts?pivotId=$pivotId&before=1&after=2") ~> topicController.routes ~> check {
              status mustBe StatusCodes.OK
              val apiResponse = responseAs[Seq[PostWithoutSecret]]
              apiResponse mustBe Seq(post.withoutSecret)
            }
          }
      }
    }
  }
}
