package io.gitlab.agluszak.forumeria.services

import io.gitlab.agluszak.forumeria.CommonTest
import io.gitlab.agluszak.forumeria.http.objects.{TopicCreateResponse, TopicDeleteResult, TopicGetPostsResult, TopicGetResult}

class TopicServiceSpec extends CommonTest {
  "TopicService" can {
    "create" which {
      "given valid data, creates a valid topic" in {
        createTopicAndDelete() { (topic, post) =>
          topic.id mustBe defined
          post.id mustBe defined
          post.topicId mustBe topic.id.get
        }
      }
    }

    "delete" which {
      "deletes a topic" in {
        topicService.create("Test post", "John", "john@doe.com", "Hi there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            topicService.delete(topic.id.get, post.secret).flatMap { result =>
              result mustBe TopicDeleteResult.Success

              topicService.size().map { n =>
                n mustBe 0
              }
              postService.size().map { n =>
                n mustBe 0
              }
            }
          }
      }

      "does not delete if the secret is incorrect" in {
        createTopicAndDelete() { (topic, post) =>
            topicService.delete(topic.id.get, post.secret + "x").flatMap { result =>
              result mustBe TopicDeleteResult.WrongSecret

              topicService.size().map { n =>
                n mustBe 1
              }
              postService.size().map { n =>
                n mustBe 1
              }
            }
          }
      }

      "deletes all posts belonging to a topic" in {
        createTopicAndDelete() { (topic, post) =>
          postService.create(topic.id.get, "John", "john@doe.com", "Hi").flatMap { _ =>
              postService.size().map(_ mustBe 2)
              topicService.delete(topic.id.get, post.secret).flatMap { result =>
                result mustBe TopicDeleteResult.Success

                topicService.size().map { n =>
                  n mustBe 0
                }
                postService.size().map { n =>
                  n mustBe 0
                }
              }
            }
          }
      }
    }

    "get" which {
      "returns existing topic" in {
        createTopicAndDelete() { (topic, _) =>
          topicService.get(topic.id.get).flatMap(_ mustBe TopicGetResult.Success(topic))
        }
      }

      "fails to return nonexistent topic" in {
        topicService.create("Test post", "John", "john@doe.com", "Hi there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            topicService.delete(topic.id.get, post.secret).flatMap { _ =>
              topicService.get(topic.id.get).flatMap(_ mustBe TopicGetResult.TopicNotFound)
            }
          }
      }
    }

    "search" which {
      "returns empty list when there are no topics" in {
        topicService.search(None, None).map(_.size mustBe 0)
      }

      "returns at most maxTopics topics" in {
        createTopicAndDelete() { (topic, post1) =>
          createPostAndDelete(topic) { post2 =>
            createPostAndDelete(topic) { post3 =>
              createPostAndDelete(topic) { post4 =>
                postService.size().map(_ mustBe 4)
                topicService.search(Some(4), None).map(_.size mustBe maxTopics.min(4))
                topicService.search(Some(2), Some(2)).map(_.size mustBe maxTopics.min(2))
                topicService.search(None, Some(4)).map(_.size mustBe 0)
              }
            }
          }
        }
      }
    }

    "getPosts" which {
      "returns list containing first post for a newly created topic" in {
        createTopicAndDelete() { (topic, post) =>
          topicService.getPosts(topic.id.get, post.id.get, 1, 1).map { result =>
            result mustBe TopicGetPostsResult.Success(Seq(post.withoutSecret))
          }
        }
      }

      "fails for invalid pivots and topics" in {
        createTopicAndDelete() { (topic, _) =>
          createTopicAndDelete() { (_, postFromAnotherTopic) =>
              topicService.getPosts(topic.id.get, postFromAnotherTopic.id.get, 1, 1).map { result =>
                result mustBe TopicGetPostsResult.PivotNotFound
              }
            topicService.create("Test post", "John", "john@doe.com", "Hi there")
              .flatMap { case TopicCreateResponse(deletedTopic, deletedPost) =>
                topicService.delete(deletedTopic.id.get, deletedPost.secret).flatMap { _ =>
                  topicService.getPosts(topic.id.get, deletedPost.id.get, 1, 1).map { result =>
                      result mustBe TopicGetPostsResult.PivotNotFound
                    }
                  topicService.getPosts(deletedTopic.id.get, deletedPost.id.get, 1, 1).map { result =>
                      result mustBe TopicGetPostsResult.TopicNotFound
                  }
                }
              }
          }
        }
      }

      "returns at most maxPosts posts" in {
          createTopicAndDelete() { (topic, post1) =>
            createPostAndDelete(topic) { post2 =>
              createPostAndDelete(topic) { post3 =>
                createPostAndDelete(topic) { post4 =>
                  postService.size().map(_ mustBe 4)
                  topicService.getPosts(topic.id.get, post3.id.get, 1, 1).map { result =>
                    inside(result) { case TopicGetPostsResult.Success(posts) =>
                      posts.size mustBe 3.min(maxPosts)
                      posts.find(_ == post3.withoutSecret) mustBe defined
                      posts.find(_ == post1.withoutSecret) must not be defined
                    }
                  }
                }
              }
            }
        }
      }
    }
  }
}
