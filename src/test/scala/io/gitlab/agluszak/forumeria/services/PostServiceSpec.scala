package io.gitlab.agluszak.forumeria.services

import io.gitlab.agluszak.forumeria.CommonTest
import io.gitlab.agluszak.forumeria.http.objects._

import scala.concurrent.Future

class PostServiceSpec extends CommonTest {

  "PostService" can {
    "create" which {
      "given valid data, creates a valid topic" in {
        createTopicAndDelete() { (topic, _) =>
          createPostAndDelete(topic) { post =>
            post.id mustBe defined
            post.topicId mustBe topic.id.get
          }
        }
      }
    }

    "delete" which {
      "deletes a post" in {
        createTopicAndDelete() { (topic, _) =>
          postService.create(topic.id.get, "John", "john@doe.com", "Hello")
            .flatMap { case PostCreateResult.Success(post) =>
              postService.delete(post.id.get, post.secret).flatMap { result =>
                result mustBe PostDeleteResult.Success
                postService.size().map { n =>
                  n mustBe 1
                }
              }
            case _ => Future.failed(new Error)
            }
        }
      }

      "does not delete if secret is incorrect" in {
        createTopicAndDelete() { (topic, _) =>
          postService.create(topic.id.get, "John", "john@doe.com", "Hello")
            .flatMap { case PostCreateResult.Success(post) =>
              postService.delete(post.id.get, post.secret + "x").flatMap { result =>
                result mustBe PostDeleteResult.WrongSecret
                postService.size().map { n =>
                  n mustBe 2
                }
              }
            case _ => Future.failed(new Error)
            }
        }
      }

      "does not delete the first post" in {
        createTopicAndDelete() { (_, post) =>
          postService.delete(post.id.get, post.secret).flatMap { result =>
            result mustBe PostDeleteResult.CannotDeleteFirstPost
            postService.size().map { n =>
              n mustBe 1
            }
          }
        }
      }
    }

    "get" which {
      "returns existing post" in {
        createTopicAndDelete() { (_, post) =>
          postService.get(post.id.get).flatMap(_ mustBe PostGetResult.Success(post.withoutSecret))
        }
      }

      "fails to return nonexistent post" in {
        topicService.create("Test post", "John", "john@doe.com", "Hi there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            topicService.delete(topic.id.get, post.secret).flatMap { _ =>
              postService.get(post.id.get).flatMap(_ mustBe PostGetResult.PostNotFound)
            }
          }
      }
    }

    "edit" which {
      "successfully returns modified post" in {
        createTopicAndDelete() { (_, post) =>
          val newMessage = "EDITED"
          postService.edit(post.id.get, newMessage, post.secret).flatMap { result =>
            inside(result) { case PostEditResult.Success(editedPost) =>
              editedPost.createdDate mustBe post.createdDate
              editedPost.editedDate.after(post.editedDate) mustBe true
              editedPost.message mustBe newMessage
            }
          }
        }
      }

      "fails if secret is incorrect" in {
        createTopicAndDelete() { (_, post) =>
          postService.edit(post.id.get, "EDITED", post.secret + "x").flatMap { result =>
            result mustBe PostEditResult.WrongSecret
          }
        }
      }

      "fails if post does not exist" in {
        topicService.create("Test post", "John", "john@doe.com", "Hi there")
          .flatMap { case TopicCreateResponse(topic, post) =>
            topicService.delete(topic.id.get, post.secret).flatMap { _ =>
              postService.edit(post.id.get, "EDITED", post.secret).flatMap(_ mustBe PostEditResult.PostNotFound)
            }
          }
      }
    }

  }
}
