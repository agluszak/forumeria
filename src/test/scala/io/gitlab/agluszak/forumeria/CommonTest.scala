package io.gitlab.agluszak.forumeria

import io.gitlab.agluszak.forumeria.http.objects.{PostCreateResult, TopicCreateResponse}
import io.gitlab.agluszak.forumeria.models.{Post, Topic}
import io.gitlab.agluszak.forumeria.repositiories.{DAO, PostRepository, TopicRepository}
import io.gitlab.agluszak.forumeria.services._
import org.scalatest._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class CommonTest extends AsyncWordSpec
  with BeforeAndAfterAll
  with MustMatchers
  with ConfigService
  with Inside {

  val databaseService = new H2Service
  val dao = new DAO(databaseService)
  val postRepository = new PostRepository(dao)
  val topicRepository = new TopicRepository(dao)
  val postService = new PostService(secretLength, databaseService, postRepository, topicRepository)
  val topicService = new TopicService(secretLength, defaultPosts, maxPosts, defaultTopics, maxTopics,
    databaseService, topicRepository, postRepository)

  override def beforeAll: Unit = {
    org.apache.log4j.BasicConfigurator.configure()
    Await.result(databaseService.db.run(dao.createTables), 10.seconds)
  }

  override def afterAll: Unit = {
    Await.result(databaseService.db.run(dao.dropTables), 10.seconds)
  }

  def createTopicAndDelete()(assertion: (Topic, Post) => Future[Assertion]): Future[Assertion] = {
    topicService.create("Test post", "John", "john@doe.com", "Hi there").flatMap {
      case TopicCreateResponse(topic, post) =>
        assertion(topic, post).flatMap { assertionResult =>
          topicService.delete(topic.id.get, post.secret).map { _ => assertionResult }
        }
    }
  }

  def createPostAndDelete(topic: Topic)(assertion: Post => Future[Assertion]): Future[Assertion] = {
    postService.create(topic.id.get, "Peter", "peter@doe.com", "Hello").flatMap {
      case PostCreateResult.Success(post) =>
        assertion(post).flatMap { assertionResult =>
          postService.delete(post.id.get, post.secret).map { _ => assertionResult }
        }
      case _ => Future.failed(new Error)
    }
  }

}