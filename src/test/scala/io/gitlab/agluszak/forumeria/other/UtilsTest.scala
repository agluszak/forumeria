package io.gitlab.agluszak.forumeria.other

import io.gitlab.agluszak.forumeria.utils.Utils
import org.scalatest.{MustMatchers, WordSpec}

class UtilsTest extends WordSpec with MustMatchers {
  "Utils" can {
    "validate email" should {
      "fail for incorrect emails" in {
        Utils.validateEmail("john") mustBe false
        Utils.validateEmail("john@@doe.") mustBe false
        Utils.validateEmail("@doe") mustBe false
      }

      "return true for correct emails" in {
        Utils.validateEmail("john@doe.com") mustBe true
        Utils.validateEmail("email.testowy@poczta.pl") mustBe true
      }
    }

    "compare strings" should {
      "fail for different strings" in {
        Utils.safeStringCompare("a", "b") mustBe false
        Utils.safeStringCompare("askdjhaksdj", "loksjdlsjdfffffffo") mustBe false
      }

      "return true for identical strings" in {
        Utils.safeStringCompare("asdasd", "asdasd") mustBe true
      }
    }
  }
}
