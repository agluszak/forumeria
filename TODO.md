# TODO
- Przerobić tworzenie bazy "z kodu" na migracje (Flyway)
- Refaktor: Przeorganizować pakiety, rozbić klasę DAO
- Przenieść odpowiednie rzeczy z Utils do modelu
- Zmienić Option[Id] na coś, co nie będzie potrzebowało brzydkich "getów" w kodzie
- Zrobić sensowną i18n 