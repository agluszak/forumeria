name := "forumeria"

version := "0.1"

scalaVersion := "2.12.4"

val akkaHttpVersion = "10.1.0-RC1"
val slickVersion = "3.2.1"
val akkaVersion = "2.5.8"
val scalatestVersion = "3.0.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "org.scalatest" %% "scalatest" % scalatestVersion % Test,
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "org.postgresql" % "postgresql" % "42.2.1",
  "org.slf4j" % "slf4j-log4j12" % "1.7.25",
  "com.h2database" % "h2" % "1.4.196" % Test
)